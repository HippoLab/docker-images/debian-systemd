Debian
======

Based on official [Debian](https://hub.docker.com/_/debian) images, target image is fully prepared to run systemd as a
pid 1 with no extra privileges 

# Contents
- [Requirements](#requirements)
- [Building](#building)
- [Usage](#usage)
- [License](#license)
- [Author Information](#author)

## <a name="requirements"></a> Requirements
Image to be built and run requires docker - containerization platform. Check [upstream documentation](https://docs.docker.com/install)
for how to install docker on your system

## <a name="building"></a> Building
To build your own image run following command in repository root path
```sh
docker build --pull --tag debian:systemd .
```

## <a name="usage"></a> Usage
Using command line:
```sh
docker run \
  --detach \
  --name=debian \
  --mount type=bind,source=/sys/fs/cgroup,target=/sys/fs/cgroup \
  --mount type=bind,source=/sys/fs/fuse,target=/sys/fs/fuse \
  --mount type=tmpfs,destination=/run \
  --mount type=tmpfs,destination=/run/lock \
  registry.gitlab.com/hippolab/docker_images/debian:systemd
```
Using Ansible:
```yaml
- name: Start a container
  docker_container:
    name: debian
    image: registry.gitlab.com/hippolab/docker_images/debian:systemd
    state: started
    recreate: yes
    devices:
      /dev/fuse:/dev/fuse
    volumes:
      - /sys/fs/cgroup:/sys/fs/cgroup
    tmpfs:
      - /tmp
      - /run
      - /run/lock
```
To validate run following command
```sh
docker exec debian systemctl status
```

## <a name="license"></a> License
The module is distributed under [MIT License](LICENSE.txt). Please make sure you have read, understood and agreed to it
terms and conditions

## <a name="author"></a> Author Information
HippoLab <v.tiukhtin@gmail.com><br/>London, UK
